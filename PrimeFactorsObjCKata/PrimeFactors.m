#import "PrimeFactors.h"

@implementation PrimeFactors

+ (NSArray *)generate:(int)num {
    NSMutableArray *primes = [NSMutableArray array];

    for (int divisor = 2; num > 1; divisor++)
        for (; num % divisor == 0; num /= divisor)
            [primes addObject:[NSNumber numberWithInt:divisor]];

    return primes;
}


@end
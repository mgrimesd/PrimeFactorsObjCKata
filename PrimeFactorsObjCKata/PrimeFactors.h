#import <Foundation/Foundation.h>

@interface PrimeFactors : NSObject

+ (NSArray *)generate:(int)num;

@end
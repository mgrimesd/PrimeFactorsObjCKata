#import "TestHelper.h"
#import "PrimeFactors.h"

SpecBegin(PrimeFactorsTest)

describe(@"PrimeFactors", ^{

    it(@"should factor 1", ^{
        expect([PrimeFactors generate:1]).to.equal(@[]);
    });

    it(@"should factor 2", ^{
        expect([PrimeFactors generate:2]).to.equal(@[@2]);
    });

    it(@"should factor 3", ^{
        expect([PrimeFactors generate:3]).to.equal(@[@3]);
    });

    it(@"should factor 4", ^{
        // Expecta 0.2.3 doesn't grok equal(@[@2, @2]), but will consume NSArray.
        NSArray *factors = @[@2, @2];
        expect([PrimeFactors generate:4]).to.equal(factors);
    });

    it(@"should factor 6", ^{
        NSArray *factors = @[@2, @3];
        expect([PrimeFactors generate:6]).to.equal(factors);
    });

    it(@"should factor 8", ^{
        NSArray *factors = @[@2, @2, @2];
        expect([PrimeFactors generate:8]).to.equal(factors);
    });

    it(@"should factor 9", ^{
        NSArray *factors = @[@3, @3];
        expect([PrimeFactors generate:9]).to.equal(factors);
    });

});

SpecEnd